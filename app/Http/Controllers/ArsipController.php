<?php

namespace App\Http\Controllers;

use App\Models\JenisSuratModel;
use App\Models\SuratKeluarModel;
use App\Models\SuratMasukModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArsipController extends Controller
{
    public function index()
    {
        $jenis = JenisSuratModel::all();
        if (Auth::user()->level === 'RENMIN' || Auth::user()->level === 'KABID') {
            $masuk = SuratMasukModel::with(['rJenis', 'rDisposisi'])
                ->whereHas('rDisposisi')
                ->get();
            $keluar = SuratKeluarModel::with(['rJenis', 'rUser'])->where('crossCheck', 'Approve')
                ->get();
            $masukC = SuratMasukModel::whereHas('rDisposisi')->count();
            $keluarC = SuratKeluarModel::where('crossCheck', 'Approve')->where('status', 'acc')->count();
        } else {
            $masuk = SuratMasukModel::with(['rJenis','rDisposisi','rDisposisi.rAkses'])
                ->whereHas('rDisposisi')
                ->whereHas('rDisposisi.rAkses', function ($q) {
                    $q->where('level', Auth::user()->level);
                })->get();
            $keluar = SuratKeluarModel::with('rJenis')->where('crossCheck', 'Approve')
                ->whereHas('rUser', function ($q) {
                    $q->where('level', Auth::user()->level);
                })
                ->get();
            $masukC = SuratMasukModel::with(['rJenis','rDisposisi','rDisposisi.rAkses'])
                ->whereHas('rDisposisi')
                ->whereHas('rDisposisi.rAkses', function ($q) {
                    $q->where('level', Auth::user()->level);
                })->count();
            $keluarC = SuratKeluarModel::with('rJenis')->where('crossCheck', 'Approve')
                ->whereHas('rUser', function ($q) {
                    $q->where('level', Auth::user()->level);
                })
                ->count();
        }
//        dd($masuk);
        return view('surat.arsip.index', compact('masuk', 'keluar', 'masukC', 'keluarC', 'jenis'));
    }

    public function filterIndex(Request $request)
    {
        $jenis = JenisSuratModel::all();
        if (Auth::user()->level === 'RENMIN' || Auth::user()->level === 'KABID') {
            $masuk = SuratMasukModel::with(['rJenis', 'rDisposisi'])
                ->whereHas('rDisposisi')
                ->whereHas('rJenis', function ($q) use ($request) {
                    $q->where('idJenisSurat', $request->jenis);
                })
                ->whereHas('rDisposisi.rAkses', function ($q) use ($request) {
                    $q->where('level', $request->subbid);
                })
//                ->whereMonth('tglMasukSurat', $request->bulan)
//                ->whereYear('tglMasukSurat', $request->tahun)
                ->get();
            $keluar = SuratKeluarModel::with(['rJenis', 'rUser'])->where('crossCheck', 'Approve')
                ->whereHas('rJenis', function ($q) use ($request) {
                    $q->where('idJenisSurat', $request->jenis);
                })
                ->whereHas('rUser', function ($q) use ($request) {
                    $q->where('level', $request->subbid);
                })
//                ->whereMonth('tglSuratKeluar', $request->bulan)
//                ->whereYear('tglSuratKeluar', $request->tahun)
                ->get();
            $masukC = SuratMasukModel::whereHas('rDisposisi')->count();
            $keluarC = SuratKeluarModel::where('crossCheck', 'Approve')->where('status', 'acc')->count();
        } else {
            $masuk = SuratMasukModel::with(['rJenis', 'rDisposisi.rAkses'])
                ->whereHas('rDisposisi')
                ->whereHas('rJenis', function ($q) use ($request) {
                    $q->where('idJenisSurat', $request->jenis);
                })
//                ->whereMonth('tglMasukSurat', $request->bulan)
//                ->whereYear('tglMasukSurat', $request->tahun)
                ->get();
            $keluar = SuratKeluarModel::with('rJenis')->where('crossCheck', 'Approve')
                ->whereHas('rJenis', function ($q) use ($request) {
                    $q->where('idJenisSurat', $request->jenis);
                })
                ->where('idPengguna', Auth::user()->idPengguna)
//                ->whereMonth('tglSuratKeluar', $request->bulan)
//                ->whereYear('tglSuratKeluar', $request->tahun)
                ->get();
            $masukC = SuratMasukModel::with(['rJenis','rDisposisi','rDisposisi.rAkses'])
                ->whereHas('rDisposisi')
                ->whereHas('rDisposisi.rAkses', function ($q) {
                    $q->where('level', Auth::user()->level);
                })->count();
            $keluarC = SuratKeluarModel::with('rJenis')->where('crossCheck', 'Approve')
                ->whereHas('rUser', function ($q) {
                    $q->where('level', Auth::user()->level);
                })
                ->count();
        }
//        dd($masuk);
        return view('surat.arsip.filter', compact('masuk', 'keluar', 'masukC', 'keluarC', 'jenis'));
    }
}
