<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PenggunaController extends Controller
{
    public function index()
    {
        $data = User::where('username', '!=', 'admin')->get();
        $renmin = User::where('username', '!=', 'admin')->where('level', 'RENMIN')->count();
        $kabid = User::where('username', '!=', 'admin')->where('level', 'KABID')->count();
        $tekkom = User::where('username', '!=', 'admin')->where('level', 'TEKKOM')->count();
        $tekinfo = User::where('username', '!=', 'admin')->where('level', 'TEKINFO')->count();
//        dd($renmin)
        return view('master.pengguna.index', compact('data', 'renmin', 'kabid', 'tekinfo', 'tekkom'));
    }

    public function add(Request $request)
    {
        $cekNrp = User::where('nrp', $request->nrp)->first();
        $cekUser = User::where('username', $request->username)->first();

        if ($cekNrp) {
            return redirect()->back()->with('alert', 'NRP sudah ada!');
        }
        if ($cekUser) {
            return redirect()->back()->with('alert', 'Username sudah ada!');
        }

        $add = User::create([
            'nrp' => $request->nrp,
            'namaPengguna' => $request->nama,
            'username' => $request->username,
            'email' => rand(1000, 0000) . '@mail.com',
            'level' => $request->level,
            'password' => Hash::make($request->password)
        ]);
        if ($add) {
            return redirect('/pengguna')->with('success', 'Data berhasil tersimpan.');
        }
        return redirect()->back()->with('alert', 'Ada kesalahan!');
    }

    public function edit(Request $request)
    {
        $cekNrp = User::where('nrp', $request->nrp)->where('idPengguna', '!=', $request->id)->first();
        $cekUser = User::where('username', $request->username)->where('idPengguna', '!=', $request->id)->first();

        if ($cekNrp) {
            return redirect()->back()->with('alert', 'NRP sudah ada!');
        }
        if ($cekUser) {
            return redirect()->back()->with('alert', 'Username sudah ada!');
        }

        if ($request->password) {
            $edit = User::find($request->id)->update([
                'nrp' => $request->nrp,
                'namaPengguna' => $request->nama,
                'username' => $request->username,
                'level' => $request->level,
                'password' => Hash::make($request->password)
            ]);
        } else {
            $edit = User::find($request->id)->update([
                'nrp' => $request->nrp,
                'namaPengguna' => $request->nama,
                'username' => $request->username,
                'level' => $request->level,
            ]);
        }

        if ($edit) {
            return redirect('/pengguna')->with('success', 'Data berhasil diubah.');
        }
        return redirect()->back()->with('alert', 'Ada kesalahan!');
    }

    public function del($id)
    {
        $del = User::find($id)->delete();
        if ($del) {
            return response()->json(1);
        } else {
            return response()->json(2);
        }
    }
}
