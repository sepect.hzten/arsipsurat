<?php

namespace App\Http\Controllers;

use App\Models\JenisSuratModel;
use App\Models\SuratKeluarModel;
use App\Models\SuratMasukModel;
use Illuminate\Http\Request;

class JenisController extends Controller
{
    public function index()
    {
        $data = JenisSuratModel::all();
        return view('master.jenis.index', compact('data'));
    }

    public function add(Request $request)
    {
        $cekKode = JenisSuratModel::where('kodeSurat', $request->kode)->first();
        $cekJenis = JenisSuratModel::where('jenisSurat', $request->nama)->first();
        if ($cekKode) {
            return redirect()->back()->with('alert', 'Kode jenis surat sudah ada!');
        }
        if ($cekJenis) {
            return redirect()->back()->with('alert', 'Nama jenis surat sudah ada!');
        }
        $add = JenisSuratModel::create([
            'kodeSurat' => $request->kode,
            'jenisSurat' => $request->nama,
        ]);
        if ($add) {
            return redirect('/jenis')->with('success', 'Data berhasil tersimpan.');
        }
        return redirect()->back()->with('alert', 'Ada kesalahan!');
    }

    public function edit(Request $request)
    {
        $cekKode = JenisSuratModel::where('kodeSurat', $request->kode)->where('idJenisSurat', '!=', $request->id)->first();
        $cekJenis = JenisSuratModel::where('jenisSurat', $request->nama)->where('idJenisSurat', '!=', $request->id)->first();
        if ($cekKode) {
            return redirect()->back()->with('alert', 'Kode jenis surat sudah ada!');
        }
        if ($cekJenis) {
            return redirect()->back()->with('alert', 'Nama jenis surat sudah ada!');
        }
        $edit = JenisSuratModel::find($request->id)->update([
            'kodeSurat' => $request->kode,
            'jenisSurat' => $request->nama,
        ]);
        if ($edit) {
            return redirect('/jenis')->with('success', 'Data berhasil diubah.');
        }
        return redirect()->back()->with('alert', 'Ada kesalahan!');
    }

    public function del($id)
    {
        $dt = JenisSuratModel::find($id);
        $cekM = SuratMasukModel::where('kodeJenis', $dt->kodeJenis)->first();
        $cekK = SuratKeluarModel::where('kodeJenis', $dt->kodeJenis)->first();
        if ($cekK || $cekM){
            return response()->json(3);
        }else {
            $del = JenisSuratModel::destroy($id);
            if ($del) {
                return response()->json(1);
            }
            return response()->json(2);
        }
    }
}
