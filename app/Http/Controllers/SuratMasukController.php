<?php

namespace App\Http\Controllers;

use App\Models\DisposisiModel;
use App\Models\JenisSuratModel;
use App\Models\RiwayatModel;
use App\Models\SuratMasukModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SuratMasukController extends Controller
{
    function numberToRomanRepresentation($number)
    {
        $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $returnValue = '';
        while ($number > 0) {
            foreach ($map as $roman => $int) {
                if ($number >= $int) {
                    $number -= $int;
                    $returnValue .= $roman;
                    break;
                }
            }
        }
        return $returnValue;
    }

    public function index()
    {
        if(Auth::user()->level === 'RENMIN') {
            $data = SuratMasukModel::with('rJenis')->get();
        }else{
            $data = SuratMasukModel::with(['rJenis','rDisposisi','rDisposisi.rAkses'])
                ->whereHas('rDisposisi')
                ->whereHas('rDisposisi.rAkses', function ($q) {
                    $q->where('level', Auth::user()->level);
                })->get();
        }
        return view('surat.masuk.index', compact('data'));
    }

    public function create()
    {
        $jenis = JenisSuratModel::all();
        return view('surat.masuk.add', compact('jenis'));
    }

    public function update($id)
    {
        $jenis = JenisSuratModel::all();
        $data = SuratMasukModel::find($id);
        return view('surat.masuk.edit', compact('jenis', 'data'));
    }

    public function add(Request $request)
    {
        $no = SuratMasukModel::count();
        $jenis = JenisSuratModel::find($request->jenis);
        $bulan = $this->numberToRomanRepresentation(date('m', strtotime($request->tglSurat)));
        $tahun = date('Y', strtotime($request->tglSurat));
        $agenda = $jenis->kodeSurat . '/' . $no + 1 . '/' . $bulan . '/' . $tahun;

        $cekNo = SuratMasukModel::where('noSurat', $request->noSurat)->first();
        if ($cekNo) {
            return redirect()->back()->withInput()->with('alert', 'Nomor surat sudah ada!');
//            return redirect()->back()->with('alert', 'Nomor surat sudah ada!');
        }

        $surat = '';

        if ($request->hasFile('file')) {
            $surat = 'surat-masuk-' . rand(10000, 99999) . '.' . $request->file('file')->extension();
        }

        $add = SuratMasukModel::create([
            'tglMasukSurat' => $request->tglMasuk,
            'noAgenda' => $agenda,
            'noSurat' => $request->noSurat,
            'idJenisSurat' => $request->jenis,
            'tglPembuatanSurat' => $request->tglSurat,
            'jamSurat' => $request->jam,
            'pengirim' => $request->pengirim,
            'perihal' => $request->perihal,
            'keterangan' => $request->keterangan,
            'fileSuratMasuk' => $surat,
        ]);

        if ($add) {
            RiwayatModel::create([
                'idSuratMasuk' => $add->idSuratMasuk,
                'tglSurat' => $request->tglMasuk,
                'noSurat' => $request->noSurat,
                'idJenisSurat' => $add->idJenisSurat,
                'perihal' => $request->perihal,
                'status' => 'Masuk'
            ]);
            $request->file('file')->move(public_path('file/surat/'), $surat);
            return redirect('/suratmasuk')->with('success', 'Data berhasil tersimpan.');
        }
        return redirect()->back()->with('alert', 'Ada kesalahan!');
    }

    public function edit(Request $request)
    {
        $cekNo = SuratMasukModel::where('noSurat', $request->noSurat)->where('idSuratMasuk', '!=', $request->id)->first();

        if ($cekNo) {
            return redirect()->back()->with('alert', 'Nomor surat sudah ada!');
        }

        $surat = '';

        if ($request->hasFile('file')) {
            $surat = 'surat-masuk-' . rand(10000, 99999) . '.' . $request->file('file')->extension();
        }

        $dt = SuratMasukModel::find($request->id);

        $edit = SuratMasukModel::find($request->id)->update([
            'tglMasukSurat' => $request->tglMasuk,
            'noSurat' => $request->noSurat,
            'tglPembuatanSurat' => $request->tglSurat,
            'jamSurat' => $request->jam,
            'pengirim' => $request->pengirim,
            'perihal' => $request->perihal,
            'keterangan' => $request->keterangan,
            'fileSuratMasuk' => $surat === '' ? $dt->fileSuratMasuk : $surat,
        ]);

        if ($edit) {
            if ($request->hasFile('file')) {
                $request->file('file')->move(public_path('file/surat/'), $surat);
            }
            return redirect('/suratmasuk')->with('success', 'Data berhasil diubah.');
        }
        return redirect()->back()->with('alert', 'Ada kesalahan!');
    }

    public function del($id)
    {
        $cekM = DisposisiModel::where('idSuratMasuk', $id)->first();
        if ($cekM){
            return response()->json(3);
        }else {
            $del = SuratMasukModel::destroy($id);
            if ($del) {
                return response()->json(1);
            }
            return response()->json(2);
        }
    }
}
