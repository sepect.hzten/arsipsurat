<?php

namespace App\Http\Controllers;

use App\Models\AksesDisposisiModel;
use App\Models\DisposisiModel;
use App\Models\JenisSuratModel;
use App\Models\RiwayatModel;
use App\Models\SuratMasukModel;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DisposisiController extends Controller
{
    public function index()
    {
        $data = SuratMasukModel::with('rJenis')->whereNotIn('idSuratMasuk', DisposisiModel::pluck('idSuratMasuk'))->get();
        return view('surat.disposisi.index', compact('data'));
    }

    public function create($id)
    {
        $data = SuratMasukModel::find($id);
        return view('surat.disposisi.add', compact('data'));
    }

    public function add(Request $request)
    {
        $surat = '';

        if ($request->hasFile('file')) {
            $surat = 'disposisi-' . rand(10000, 99999) . '.' . $request->file('file')->extension();
        }

        $add = DisposisiModel::create([
            'idSuratMasuk' => $request->id,
            'idPengguna' => Auth::user()->idPengguna,
            'fileDisposisi' => $surat,
        ]);

        if ($add) {
            foreach ($request->kepada as $t) {
                AksesDisposisiModel::create([
                    'idDisposisi' => $add->idDisposisi,
                    'level' => $t
                ]);
            }
            $request->file('file')->move(public_path('file/surat/'), $surat);
            return redirect('/disposisi')->with('success', 'Data berhasil tersimpan.');
        }
        return redirect()->back()->with('alert', 'Ada kesalahan!');
    }

    public function cetak($id)
    {
        $data = SuratMasukModel::find($id);
        $pdf = Pdf::loadView('surat.disposisi.cetak', ['data' => $data]);
        return $pdf->stream('disposisi' . rand(100, 999) . '.pdf');
    }
}
