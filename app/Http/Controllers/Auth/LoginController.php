<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            return redirect('/home');
        }
        return view('auth.login');
    }

    public function signIn(Request $request)
    {
        $cek = User::where('username', $request->username)->where('level', $request->level)->first();
        $password = $request->password;
        if ($cek) {
            if (!Hash::check($password, $cek->password)) {
                return redirect()->back()->with('alert', 'Mohon periksa password anda!');
            }
            if (Auth::attempt(['email' => $cek->email, 'password' => $password])) {
                if ($request->level === 'RENMIN') {
                    return redirect('/')->with('success', 'Login Berhasil');
                }
                elseif ($request->level === 'TEKKOM' || $request->level === 'TEKINFO') {
                    return redirect('/suratmasuk')->with('success', 'Login Berhasil');
                }
                elseif ($request->level === 'KABID') {
                    return redirect('/home')->with('success', 'Login Berhasil');
                }
            }
        }
        return redirect()->back()->with('info', 'Mohon periksa username anda!');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
