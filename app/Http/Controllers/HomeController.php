<?php

namespace App\Http\Controllers;

use App\Models\DisposisiModel;
use App\Models\JenisSuratModel;
use App\Models\RiwayatModel;
use App\Models\SuratKeluarModel;
use App\Models\SuratMasukModel;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $masukC = SuratMasukModel::whereHas('rDisposisi')->count();
        $keluarC = SuratKeluarModel::count();
        $petugasC = User::where('username', '!=', 'admin')->count();
        $disposisiC = DisposisiModel::count();
        $data = RiwayatModel::with(['rMasuk','rKeluar','rJenis'])->get();
        return view('template.home', compact('disposisiC', 'petugasC', 'keluarC', 'masukC','data'));
    }

    public function indexKabid()
    {
        $jenis = JenisSuratModel::all();
        $masuk = SuratMasukModel::with(['rJenis', 'rDisposisi'])
            ->whereHas('rDisposisi')
            ->get();
        $keluar = SuratKeluarModel::with(['rJenis', 'rUser'])->where('crossCheck', 'Approve')
            ->get();
        $masukC = SuratMasukModel::whereHas('rDisposisi')->count();
        $keluarC = SuratKeluarModel::where('crossCheck', 'Approve')->count();
        return view('kabid.index', compact('masuk', 'keluar', 'masukC', 'keluarC', 'jenis'));
    }

    public function filterKabid(Request $request)
    {
        $jenis = JenisSuratModel::all();
        $masuk = SuratMasukModel::with(['rJenis', 'rDisposisi'])
            ->whereHas('rDisposisi')
            ->whereHas('rJenis', function ($q) use ($request) {
                $q->where('idJenisSurat', $request->jenis);
            })
            ->whereHas('rDisposisi.rAkses', function ($q) use ($request) {
                $q->where('level', $request->subbid);
            })
//                ->whereMonth('tglMasukSurat', $request->bulan)
//                ->whereYear('tglMasukSurat', $request->tahun)
            ->get();
        $keluar = SuratKeluarModel::with(['rJenis', 'rUser'])->where('crossCheck', 'Approve')
            ->whereHas('rJenis', function ($q) use ($request) {
                $q->where('idJenisSurat', $request->jenis);
            })
            ->whereHas('rUser', function ($q) use ($request) {
                $q->where('level', $request->subbid);
            })
//                ->whereMonth('tglSuratKeluar', $request->bulan)
//                ->whereYear('tglSuratKeluar', $request->tahun)
            ->get();
        $masukC = SuratMasukModel::whereHas('rDisposisi')->count();
        $keluarC = SuratKeluarModel::where('crossCheck', 'Approve')->count();
        return view('kabid.index', compact('masuk', 'keluar', 'masukC', 'keluarC', 'jenis'));
    }
}
