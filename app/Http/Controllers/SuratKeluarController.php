<?php

namespace App\Http\Controllers;

use App\Models\JenisSuratModel;
use App\Models\RiwayatModel;
use App\Models\SuratKeluarModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SuratKeluarController extends Controller
{
    public function index()
    {
        if (Auth::user()->level === 'RENMIN') {
            $data = SuratKeluarModel::with(['rJenis', 'rUser'])
                ->where('status',null)
                ->get();
        } else {
            $data = SuratKeluarModel::with('rJenis')
                ->whereHas('rUser', function ($q) {
                    $q->where('level', Auth::user()->level);
                })
                ->get();
        }
        return view('surat.keluar.index', compact('data'));
    }

    public function create()
    {
        $jenis = JenisSuratModel::all();
        return view('surat.keluar.add', compact('jenis'));
    }

    public function update($id)
    {
        $jenis = JenisSuratModel::all();
        $data = SuratKeluarModel::find($id);
        return view('surat.keluar.edit', compact('jenis', 'data'));
    }

    public function add(Request $request)
    {
        $cekNo = SuratKeluarModel::where('noSurat', $request->noSurat)->first();
        if ($cekNo) {
            return redirect()->back()->withInput()->with('alert', 'Nomor surat sudah ada!');
        }

        $surat = '';

        if ($request->hasFile('file')) {
            $surat = 'surat-keluar-' . rand(10000, 99999) . '.' . $request->file('file')->extension();
        }

        $add = SuratKeluarModel::create([
            'tglSuratKeluar' => $request->tglSurat,
            'idPengguna' => Auth::user()->idPengguna,
            'noSurat' => $request->noSurat,
            'idJenisSurat' => $request->jenis,
            'perihal' => $request->perihal,
            'kepada' => $request->kepada,
            'crossCheck' => 'Proses',
            'fileSuratKeluar' => $surat,
        ]);

        if ($add) {
            RiwayatModel::create([
                'idSuratKeluar' => $add->idSuratKeluar,
                'tglSurat' => $request->tglSurat,
                'noSurat' => $request->noSurat,
                'idJenisSurat' => $request->jenis,
                'perihal' => $request->perihal,
                'status' => 'Keluar'
            ]);
            $request->file('file')->move(public_path('file/surat/'), $surat);
            return redirect('/suratkeluar')->with('success', 'Data berhasil tersimpan.');
        }
        return redirect()->back()->with('alert', 'Ada kesalahan!');
    }

    public function edit(Request $request)
    {
        $cekNo = SuratKeluarModel::where('noSurat', $request->noSurat)->where('idSuratKeluar', '!=', $request->id)->first();

        if ($cekNo) {
            return redirect()->back()->with('alert', 'Nomor surat sudah ada!');
        }

        $surat = '';

        if ($request->hasFile('file')) {
            $surat = 'surat-keluar-' . rand(10000, 99999) . '.' . $request->file('file')->extension();
        }

        $dt = SuratKeluarModel::find($request->id);

        $edit = SuratKeluarModel::find($request->id)->update([
            'tglSuratKeluar' => $request->tglSurat,
            'noSurat' => $request->noSurat,
            'idJenisSurat' => $request->jenis,
            'perihal' => $request->perihal,
            'kepada' => $request->kepada,
            'crossCheck' => 'Proses',
            'fileSuratKeluar' => $surat === '' ? $dt->fileSuratKeluar : $surat,
        ]);

        if ($edit) {
            if ($request->hasFile('file')) {
                $request->file('file')->move(public_path('file/surat/'), $surat);
            }
            return redirect('/suratkeluar')->with('success', 'Data berhasil diubah.');
        }
        return redirect()->back()->with('alert', 'Ada kesalahan!');
    }

    public function editFile(Request $request)
    {
        $surat = '';

        if ($request->hasFile('file')) {
            $surat = 'surat-keluar-' . rand(10000, 99999) . '.' . $request->file('file')->extension();
        }

        $dt = SuratKeluarModel::find($request->id);

        $edit = SuratKeluarModel::find($request->id)->update([
            'fileSuratKeluar' => $surat,
            'status' => 'acc',
        ]);

        if ($edit) {
            $request->file('file')->move(public_path('file/surat/'), $surat);
            return redirect('/suratkeluar')->with('success', 'Data berhasil diubah.');
        }
        return redirect()->back()->with('alert', 'Ada kesalahan!');
    }

    public function approveSurat($id)
    {
        $edit = SuratKeluarModel::find($id)->update([
            'crossCheck' => 'Approve'
        ]);

        if ($edit) {
            return response()->json(1);
        }
        return response()->json(2);
    }

    public function tolakSurat($id)
    {
        $text = explode("@", $id);
        $edit = SuratKeluarModel::find($text[0])->update([
            'crossCheck' => 'Ditolak',
            'keteranganTolak' => $text[1]
        ]);

        if ($edit) {
            return response()->json(1);
        }
        return response()->json(2);
    }

    public function del($id)
    {
        $del = SuratKeluarModel::destroy($id);
        if ($del) {
            return response()->json(1);
        }
        return response()->json(2);
    }
}
