<?php

namespace App\Http\Controllers;

use App\Models\SatkerModel;
use Illuminate\Http\Request;

class SatkerController extends Controller
{
    public function index()
    {
        $data = SatkerModel::all();
        return view('master.satker.index', compact('data'));
    }

    public function add(Request $request)
    {
        $cekKode = SatkerModel::where('kodeSatker', $request->kode)->first();
        $cekJenis = SatkerModel::where('namaSatker', $request->nama)->first();
        if ($cekKode) {
            return redirect()->back()->with('alert', 'Kode Satker sudah ada!');
        }
        if ($cekJenis) {
            return redirect()->back()->with('alert', 'Nama Satker sudah ada!');
        }
        $add = SatkerModel::create([
            'kodeSatker' => $request->kode,
            'namaSatker' => $request->nama,
        ]);
        if ($add) {
            return redirect('/satker')->with('success', 'Data berhasil tersimpan.');
        }
        return redirect()->back()->with('alert', 'Ada kesalahan!');
    }

    public function edit(Request $request)
    {
        $cekKode = SatkerModel::where('kodeSatker', $request->kode)->where('idnamaSatker', '!=', $request->id)->first();
        $cekJenis = SatkerModel::where('namaSatker', $request->nama)->where('idnamaSatker', '!=', $request->id)->first();
        if ($cekKode) {
            return redirect()->back()->with('alert', 'Kode Satker sudah ada!');
        }
        if ($cekJenis) {
            return redirect()->back()->with('alert', 'Nama Satker sudah ada!');
        }
        $edit = SatkerModel::find($request->id)->update([
            'kodeSatker' => $request->kode,
            'namaSatker' => $request->nama,
        ]);
        if ($edit) {
            return redirect('/satker')->with('success', 'Data berhasil diubah.');
        }
        return redirect()->back()->with('alert', 'Ada kesalahan!');
    }

    public function del($id)
    {
        $del = SatkerModel::destroy($id);
        if ($del) {
            return response()->json(1);
        }
        return response()->json(2);
    }
}
