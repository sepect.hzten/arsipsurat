<?php

namespace App\Models;

use App\Traits\UsesUUID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AksesDisposisiModel extends Model
{
    use UsesUUID;

    protected $table = 'akses_disposisi';
    protected $primaryKey = 'idAkses';

    protected $fillable = [
        'idDisposisi',
        'level',
    ];
}
