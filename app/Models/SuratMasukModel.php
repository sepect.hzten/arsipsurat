<?php

namespace App\Models;

use App\Traits\UsesUUID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class SuratMasukModel extends Model
{
    use UsesUUID;

    protected $table = 'surat_masuk';
    protected $primaryKey = 'idSuratMasuk';

    protected $fillable = [
        'tglMasukSurat',
        'noAgenda',
        'noSurat',
        'jamSurat',
        'idJenisSurat',
        'tglPembuatanSurat',
        'pengirim',
        'perihal',
        'keterangan',
        'fileSuratMasuk',
    ];

    public function rJenis(): BelongsTo
    {
        return $this->belongsTo(JenisSuratModel::class, 'idJenisSurat');
    }

    public function rDisposisi(): HasOne
    {
        return $this->hasOne(DisposisiModel::class, 'idSuratMasuk');
    }
}
