<?php

namespace App\Models;

use App\Traits\UsesUUID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class RiwayatModel extends Model
{
    use UsesUUID;

    protected $table = 'riwayat_surat';
    protected $primaryKey = 'idRiwayat';

    protected $fillable = [
        'idSuratMasuk',
        'idSuratKeluar',
        'tglSurat',
        'noSurat',
        'idJenisSurat',
        'perihal',
        'jenisSurat',
        'status'
    ];

    public function rMasuk(): BelongsTo
    {
        return $this->belongsTo(SuratMasukModel::class, 'idSuratMasuk');
    }

    public function rKeluar(): BelongsTo
    {
        return $this->belongsTo(SuratKeluarModel::class, 'idSuratKeluar');
    }

    public function rJenis(): BelongsTo
    {
        return $this->belongsTo(JenisSuratModel::class, 'idJenisSurat');
    }
}
