<?php

namespace App\Models;

use App\Traits\UsesUUID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SuratDisposisiModel extends Model
{
    use UsesUUID;

    protected $table = 'surat_disposisi';
    protected $primaryKey = 'idSuratDisposisi';

    protected $fillable = [
        'tglDisposisi',
        'jam',
        'suratDari',
        'noSurat',
        'tglSurat',
        'perihal',
    ];
}
