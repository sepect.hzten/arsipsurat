<?php

namespace App\Models;

use App\Traits\UsesUUID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisSuratModel extends Model
{
    use UsesUUID;

    protected $table = 'jenis_surat';
    protected $primaryKey = 'idJenisSurat';

    protected $fillable = [
        'kodeSurat',
        'jenisSurat',
    ];
}
