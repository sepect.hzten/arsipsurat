<?php

namespace App\Models;

use App\Traits\UsesUUID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SatkerModel extends Model
{
    use UsesUUID;

    protected $table = 'satker';
    protected $primaryKey = 'idSatker';
    protected $fillable = [
        'kodeSatker',
        'namaSatker',
        'ruangan',
    ];
}
