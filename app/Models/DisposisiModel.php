<?php

namespace App\Models;

use App\Traits\UsesUUID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class DisposisiModel extends Model
{
    use UsesUUID;

    protected $table = 'disposisi';
    protected $primaryKey = 'idDisposisi';

    protected $fillable = [
        'idSuratMasuk',
        'idPengguna',
        'fileDisposisi'
    ];

    public function rMasuk(): BelongsTo
    {
        return $this->belongsTo(SuratMasukModel::class, 'idSuratMasuk');
    }

    public function rUser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'idPengguna');
    }

    public function rAkses(): HasMany
    {
        return $this->hasMany(AksesDisposisiModel::class, 'idDisposisi');
    }
}
