<?php

namespace App\Models;

use App\Traits\UsesUUID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SuratKeluarModel extends Model
{
    use UsesUUID;

    protected $table = 'surat_keluar';
    protected $primaryKey = 'idSuratKeluar';
    protected $fillable = [
        'tglSuratKeluar',
        'noSurat',
        'idPengguna',
        'idJenisSurat',
        'kepada',
        'perihal',
        'crossCheck',
        'status',
        'keteranganTolak',
        'fileSuratKeluar',
    ];

    public function rJenis(): BelongsTo
    {
        return $this->belongsTo(JenisSuratModel::class, 'idJenisSurat');
    }

    public function rUser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'idPengguna');
    }
}
