<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::create([
            'nrp' => '12345',
            'namaPengguna' => 'admin',
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'level' => 'RENMIN',
            'password' => Hash::make('admin')
        ]);
    }
}
