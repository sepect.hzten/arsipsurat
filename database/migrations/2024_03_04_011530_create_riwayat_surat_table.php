<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('riwayat_surat', function (Blueprint $table) {
            $table->uuid('idRiwayat')->primary();
            $table->uuid('idSuratMasuk')->nullable();
            $table->uuid('idSuratKeluar')->nullable();
            $table->date('tglSurat');
            $table->uuid('idJenisSurat');
            $table->string('noSurat');
            $table->string('perihal');
            $table->enum('status',['Masuk','Keluar']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('riwayat_surat');
    }
};
