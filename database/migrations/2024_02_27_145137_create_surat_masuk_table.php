<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('surat_masuk', function (Blueprint $table) {
            $table->uuid('idSuratMasuk')->primary();
            $table->uuid('idJenisSurat');
            $table->date('tglMasukSurat');
            $table->time('jamSurat');
            $table->string('noAgenda');
            $table->string('noSurat');
            $table->date('tglPembuatanSurat');
            $table->string('pengirim');
            $table->text('perihal');
            $table->text('keterangan');
            $table->string('fileSuratMasuk');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('surat_masuk');
    }
};
