<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('surat_keluar', function (Blueprint $table) {
            $table->uuid('idSuratKeluar')->primary();
            $table->uuid('idPengguna');
            $table->uuid('idJenisSurat');
            $table->date('tglSuratKeluar');
            $table->string('noSurat');
            $table->string('kepada');
            $table->string('perihal');
            $table->enum('crossCheck',['Approve','Proses','Ditolak']);
            $table->text('keteranganTolak')->nullable();
            $table->string('status')->nullable();
            $table->string('fileSuratKeluar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('surat_keluar');
    }
};
