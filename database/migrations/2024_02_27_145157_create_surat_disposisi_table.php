<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('surat_disposisi', function (Blueprint $table) {
            $table->uuid('idSuratDisposisi')->primary();
            $table->date('tglDisposisi');
            $table->time('jam');
            $table->string('suratDari');
            $table->string('noSurat');
            $table->date('tglSurat');
            $table->text('perihal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('surat_disposisi');
    }
};
