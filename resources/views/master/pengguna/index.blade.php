@extends('template.app')
@section('content')

    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Data Pengguna</h5>
                            <hr>
                            <div class="row">
                                <div class="col-xxl col-sm-3">
                                    <div class="card overflow-hidden">
                                        <div class="card-body">
                                            <div class="avatar-sm float-end">
                                                <div class="avatar-title bg-info-subtle text-info fs-3xl rounded">
                                                    <i class="ph ph-users"></i>
                                                </div>
                                            </div>
                                            <h4><span class="counter-value" data-target="{{$renmin}}">{{$renmin}}</span></h4>
                                            <h5 class="text-muted mb-4">RENMIN</h5>
                                        </div>
                                        <div class="progress progress-sm rounded-0" role="progressbar"
                                             aria-valuenow="100" aria-valuemin="100" aria-valuemax="100">
                                            <div class="progress-bar bg-info" style="width: 100%"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl col-sm-3">
                                    <div class="card overflow-hidden">
                                        <div class="card-body">
                                            <div class="avatar-sm float-end">
                                                <div class="avatar-title bg-danger-subtle text-info fs-3xl rounded">
                                                    <i class="ph ph-users"></i>
                                                </div>
                                            </div>
                                            <h4><span class="counter-value" data-target="{{$kabid}}">{{$kabid}}</span></h4>
                                            <h5 class="text-muted mb-4">KABID</h5>
                                        </div>
                                        <div class="progress progress-sm rounded-0" role="progressbar"
                                             aria-valuenow="100" aria-valuemin="100" aria-valuemax="100">
                                            <div class="progress-bar bg-danger" style="width: 100%"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl col-sm-3">
                                    <div class="card overflow-hidden">
                                        <div class="card-body">
                                            <div class="avatar-sm float-end">
                                                <div class="avatar-title bg-success-subtle text-info fs-3xl rounded">
                                                    <i class="ph ph-users"></i>
                                                </div>
                                            </div>
                                            <h4><span class="counter-value" data-target="{{$tekkom}}">{{$tekkom}}</span></h4>
                                            <h5 class="text-muted mb-4">TEKKOM</h5>
                                        </div>
                                        <div class="progress progress-sm rounded-0" role="progressbar"
                                             aria-valuenow="100" aria-valuemin="100" aria-valuemax="100">
                                            <div class="progress-bar bg-success" style="width: 100%"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl col-sm-3">
                                    <div class="card overflow-hidden">
                                        <div class="card-body">
                                            <div class="avatar-sm float-end">
                                                <div class="avatar-title bg-warning-subtle text-info fs-3xl rounded">
                                                    <i class="ph ph-users"></i>
                                                </div>
                                            </div>
                                            <h4><span class="counter-value" data-target="{{$tekinfo}}">{{$tekinfo}}</span></h4>
                                            <h5 class="text-muted mb-4">TEKINFO</h5>
                                        </div>
                                        <div class="progress progress-sm rounded-0" role="progressbar"
                                             aria-valuenow="100" aria-valuemin="100" aria-valuemax="100">
                                            <div class="progress-bar bg-warning" style="width: 100%"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <button type="button" class="btn btn-primary" data-bs-target="#add" data-bs-toggle="modal">
                                Tambah data
                            </button>
                        </div>
                        <div class="card-body">
                            <table id="example"
                                   class="table table-bordered dt-responsive nowrap table-striped align-middle"
                                   style="width:100%">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>NRP</th>
                                    <th>Nama</th>
                                    <th>Username</th>
                                    <th>Level</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $no =1;
                                @endphp
                                @foreach($data as $t)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$t->nrp}}</td>
                                        <td>{{$t->namaPengguna}}</td>
                                        <td>{{$t->username}}</td>
                                        <td>{{$t->level}}</td>
                                        <td>
                                            <div class="hstack gap-3 fs-base">
                                                <a href="javascript:void(0);" class="link-primary"
                                                   data-bs-toggle="modal" data-bs-target="#edit"
                                                   data-id="{{$t->idPengguna}}" data-nama="{{$t->namaPengguna}}"
                                                   data-nrp="{{$t->nrp}}"
                                                   data-username="{{$t->username}}" data-level="{{$t->level}}"><i
                                                    class="ri-edit-2-fill"></i></a>
                                                <a href="javascript:void(0);" onclick="delData('{{$t->idPengguna}}')"
                                                   class="link-danger"><i class="ri-delete-bin-5-line"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!--end col-->
            </div>
        </div>
        <!-- container-fluid -->
    </div>

    <div class="modal fade" id="add" tabindex="-1" aria-labelledby="add">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="add">Tambah Pengguna</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{url('pengguna/add')}}" method="post">
                        @csrf
                        <div class="row g-3">
                            <div class="col-xxl-6">
                                <div>
                                    <label for="nrp" class="form-label">NRP</label>
                                    <input type="text" class="form-control" id="nrp" placeholder="NRP"
                                           name="nrp" required>
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-xxl-6">
                                <div>
                                    <label for="nama" class="form-label">Nama Pengguna</label>
                                    <input type="text" class="form-control" id="nama" placeholder="Nama Satker"
                                           name="nama"
                                           required maxlength="30">
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-xxl-6">
                                <div>
                                    <label for="username" class="form-label">Username</label>
                                    <input type="text" class="form-control" id="username" placeholder="Username"
                                           name="username" required>
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-xxl-6">
                                <div>
                                    <label for="password" class="form-label">Password</label>
                                    <input type="password" class="form-control" id="password" placeholder="Password"
                                           name="password" required>
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-xxl-12">
                                <div>
                                    <label for="level" class="form-label">Level Akses</label>
                                    <select required name="level" id="level" class="form-control">
                                        <option value="" selected disabled>Pilih Level Akses</option>
                                        <option value="RENMIN">RENMIN</option>
                                        <option value="KABID">KABID</option>
                                        <option value="TEKKOM">TEKKOM</option>
                                        <option value="TEKINFO">TEKINFO</option>
                                    </select>
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-lg-12">
                                <div class="hstack gap-2 justify-content-end">
                                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                            <!--end col-->
                        </div>
                        <!--end row-->
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit" tabindex="-1" aria-labelledby="edit">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="add">Edit Satker</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{url('pengguna/edit')}}" method="post">
                        @csrf
                        <input type="hidden" id="id" name="id">
                        <div class="row g-3">
                            <div class="col-xxl-6">
                                <div>
                                    <label for="nrp" class="form-label">NRP</label>
                                    <input type="text" class="form-control" id="nrp" placeholder="NRP"
                                           name="nrp" required>
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-xxl-6">
                                <div>
                                    <label for="nama" class="form-label">Nama Pengguna</label>
                                    <input type="text" class="form-control" id="nama" placeholder="Nama Satker"
                                           name="nama"
                                           required maxlength="30">
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-xxl-6">
                                <div>
                                    <label for="username" class="form-label">Username</label>
                                    <input type="text" class="form-control" id="username" placeholder="Username"
                                           name="username" required>
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-xxl-6">
                                <div>
                                    <label for="password" class="form-label">Password</label>
                                    <input type="password" class="form-control" id="password" placeholder="Password"
                                           name="password" required>
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-xxl-12">
                                <div>
                                    <label for="level" class="form-label">Level Akses</label>
                                    <select required name="level" id="level" class="form-control">
                                        <option value="" selected disabled>Pilih Level Akses</option>
                                        <option value="RENMIN">RENMIN</option>
                                        <option value="KABID">KABID</option>
                                        <option value="TEKKOM">TEKKOM</option>
                                        <option value="TEKINFO">TEKINFO</option>
                                    </select>
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-lg-12">
                                <div class="hstack gap-2 justify-content-end">
                                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                            <!--end col-->
                        </div>
                        <!--end row-->
                    </form>
                </div>
            </div>
        </div>
    </div>

    @push('script')
        <script src="{{asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script>
            $("#edit").on("show.bs.modal", function (event) {
                let button = $(event.relatedTarget);
                let id = button.data("id");
                let nrp = button.data("nrp");
                let nama = button.data("nama");
                let level = button.data("level");
                let username = button.data("username");
                let modal = $('#edit');
                modal.find("#id").val(id);
                modal.find("#nrp").val(nrp);
                modal.find("#nama").val(nama);
                modal.find("#level").val(level);
                modal.find("#username").val(username);
            });

            function delData(id) {
                Swal.fire({
                    title: 'Hapus data',
                    text: "Yakin ingin mengapus data ini?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, hapus data!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.isConfirmed) {
                        axios.delete('{{url('pengguna/del')}}/' + id)
                            .then(function (response) {
                                if (response.data == 1) {
                                    Swal.fire({
                                        title: 'Terhapus',
                                        text: "Data berhasil terhapus.",
                                        icon: 'success',
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'Oke'
                                    }).then((result) => {
                                        location.reload(true);
                                    });
                                } else {
                                    Swal.fire({
                                        title: 'Gagal!',
                                        text: "Ada Kesalahan!",
                                        icon: 'info',
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'Oke'
                                    }).then((result) => {
                                        location.reload(true);
                                    });
                                }
                            });
                    }
                })
            }
        </script>
    @endpush
@endsection
