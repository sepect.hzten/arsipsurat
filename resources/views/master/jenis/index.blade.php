@extends('template.app')
@section('content')

    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Data Jenis Surat</h5>
                            <hr>
                            <button type="button" class="btn btn-primary" data-bs-target="#add" data-bs-toggle="modal">
                                Tambah data
                            </button>
                        </div>
                        <div class="card-body">
                            <table id="example"
                                   class="table table-bordered dt-responsive nowrap table-striped align-middle"
                                   style="width:100%">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Kode Surat</th>
                                    <th>Nama Jenis Surat</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $no =1;
                                @endphp
                                @foreach($data as $t)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$t->kodeSurat}}</td>
                                        <td>{{$t->jenisSurat}}</td>
                                        <td>
                                            <div class="hstack gap-3 fs-base">
                                                <a href="javascript:void(0);" class="link-primary"
                                                   data-bs-toggle="modal" data-bs-target="#edit"
                                                   data-id="{{$t->idJenisSurat}}" data-nama="{{$t->jenisSurat}}"
                                                   data-kode="{{$t->kodeSurat}}"><i class="ri-edit-2-fill"></i></a>
                                                <a href="javascript:void(0);" onclick="delData('{{$t->idJenisSurat}}')"
                                                   class="link-danger"><i class="ri-delete-bin-5-line"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!--end col-->
            </div>
        </div>
        <!-- container-fluid -->
    </div>

    <div class="modal fade" id="add" tabindex="-1" aria-labelledby="add">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="add">Tambah jenis surat</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{url('jenis/add')}}" method="post">
                        @csrf
                        <div class="row g-3">
                            <div class="col-xxl-6">
                                <div>
                                    <label for="kode" class="form-label">Kode Jenis Surat</label>
                                    <input type="text" class="form-control" id="kode" placeholder="Kode Surat"
                                           name="kode" required maxlength="5">
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-xxl-6">
                                <div>
                                    <label for="nama" class="form-label">Nama Jenis Surat</label>
                                    <input type="text" class="form-control" id="nama" placeholder="Nama Jenis Surat"
                                           name="nama"
                                           required maxlength="30">
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-lg-12">
                                <div class="hstack gap-2 justify-content-end">
                                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                            <!--end col-->
                        </div>
                        <!--end row-->
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit" tabindex="-1" aria-labelledby="edit">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="add">Edit jenis surat</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{url('jenis/edit')}}" method="post">
                        @csrf
                        <input type="hidden" id="id" name="id">
                        <div class="row g-3">
                            <div class="col-xxl-6">
                                <div>
                                    <label for="kode" class="form-label">Kode Jenis Surat</label>
                                    <input type="text" class="form-control" id="kode" placeholder="Kode Surat"
                                           name="kode" required maxlength="5">
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-xxl-6">
                                <div>
                                    <label for="nama" class="form-label">Nama Jenis Surat</label>
                                    <input type="text" class="form-control" id="nama" placeholder="Nama Jenis Surat"
                                           name="nama"
                                           required maxlength="30">
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-lg-12">
                                <div class="hstack gap-2 justify-content-end">
                                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                            <!--end col-->
                        </div>
                        <!--end row-->
                    </form>
                </div>
            </div>
        </div>
    </div>

    @push('script')
        <script src="{{asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script>
            $("#edit").on("show.bs.modal", function (event) {
                let button = $(event.relatedTarget);
                let id = button.data("id");
                let kode = button.data("kode");
                let nama = button.data("nama");
                let modal = $('#edit');
                modal.find("#id").val(id);
                modal.find("#kode").val(kode);
                modal.find("#nama").val(nama);
            });

            function delData(id) {
                Swal.fire({
                    title: 'Hapus data',
                    text: "Yakin ingin mengapus data ini?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, hapus data!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.isConfirmed) {
                        axios.delete('{{url('jenis/del')}}/' + id)
                            .then(function (response) {
                                if (response.data === 1) {
                                    Swal.fire({
                                        title: 'Terhapus',
                                        text: "Data berhasil terhapus.",
                                        icon: 'success',
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'Oke'
                                    }).then((result) => {
                                        location.reload(true);
                                    });
                                } else if (response.data === 2) {
                                    Swal.fire({
                                        title: 'Gagal!',
                                        text: "Ada Kesalahan!",
                                        icon: 'info',
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'Oke'
                                    }).then((result) => {
                                        location.reload(true);
                                    });
                                } else {
                                    Swal.fire({
                                        title: 'Gagal!',
                                        text: "Data terpakai di surat masuk!",
                                        icon: 'info',
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'Oke'
                                    }).then((result) => {
                                        location.reload(true);
                                    });
                                }
                            });
                    }
                })
            }
        </script>
    @endpush
@endsection
