<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Surat Muatan</title>
    <style>
        @page {
            size: A4 portrait;
        }

        html, body {
            font-family: Arial, Helvetica, sans-serif;
            min-height: 100vh;
            display: flex;
            margin: 0;
            padding: 0;
        }

        .text-header {
            font-size: 20px;
            font-weight: bold;
            margin-bottom: -20px;
        }

        .text-kode {
            font-size: 18px;
            font-weight: bold;
            margin-bottom: -15px;
        }

        .text-sub {
            font-size: 15px;
            margin-bottom: -15px;
        }

        .text-judul {
            font-size: 11px;
            margin-bottom: -10px;
        }

        .text-konten {
            font-size: 11px;
            margin-bottom: -10px;
            padding: 10px;
        }

        .text-subjudul {
            font-size: 10px;
            margin-bottom: -10px;
        }

        .table {
            border: 1px solid;
            border-collapse: collapse;
            font-size: 10px;
        }

        .table-biaya {
            font-size: 13px;
        }
    </style>
</head>
<body>
<div style="display: flex; height: 100%;">
    <div style="height:45%;padding: 10px;display: flex">
        <div style="height:100%;border: 1px solid; position: relative;">
            <div style="position: absolute;width: 40%;">
                <div style="margin-top: 15px;margin-left: 10px;text-align: center">
                    <p class="text-judul">KEPOLISIAN NEGARA REPUBLIK INDONESIA</p>
                    <p class="text-judul">DAERAH SULAWESI SELATAN</p>
                    <p class="text-judul" style="border-bottom: 1px solid black;">BIDANG TEKNOLOGI INFORMASI DAN
                        KOMUNIKASI</p>
                </div>
            </div>
            <div style="position: absolute;width: 25%;right: 10px;top: 20px">
                <div style="margin-top: 15px;margin-left: 10px;text-align: left">
                    <p class="text-subjudul">Klasifikasi : Biasa/Rahasia</p>
                    <p class="text-subjudul">Derajat : Kilat</p>
                </div>
            </div>
            <div style="position: absolute;width: 100%;top: 70px;display: flex;justify-content: center">
                <p class="text-judul"
                   style="font-weight: bold;border-bottom: 1px solid black;width: 107px;margin: 0 auto">LEMBAR
                    DISPOSISI</p>
            </div>
            <div style="position: absolute;width: 40%;top: 80px">
                <div style="margin-top: 15px;margin-left: 20px;">
                    <p class="text-subjudul">No. Agenda : {{$data->noAgenda}}</p>
                </div>
            </div>
            <div style="position: absolute;width: 25%;top: 80px;right: 10px">
                <div style="margin-top: 15px;margin-left: 10px;">
                    <p class="text-subjudul">Diterima : {{$data->tglMasukSurat}}</p>
                    <p class="text-subjudul">Jam : {{substr($data->jamSurat,0,5)}} Wita</p>
                </div>
            </div>

            <div style="width: 50%;margin-top: 125px;margin-left: 5px">
                <div style="outline: solid 1px black;text-align: center">
                    <p class="text-judul">CAT. RENMIN</p>
                </div>
                <div style="outline: solid 1px black;text-align: left;height: 76%;">
                    <p class="text-konten">SURAT DARI : {{$data->pengirim}}</p>
                    <p class="text-konten">NOMOR SURAT : {{$data->noSurat}}</p>
                    <p class="text-konten">TANGGAL SURAT : {{$data->tglPembuatanSurat}}</p>
                    <p class="text-konten">PERIHAL : {{$data->perihal}}</p>
                </div>
                <div style="outline: solid 1px black;text-align: center">
                    <p class="text-judul">Diteruskan kepada:</p>
                </div>
                <div style="outline: solid 1px black;text-align: left">
                    <p class="text-judul" style="margin-left: 5px">KASUBBID TEKKOM</p>
                </div>
                <div style="outline: solid 1px black;text-align: left">
                    <p class="text-judul" style="margin-left: 5px">Plt. KASUBBID TEKINFO</p>
                </div>
                <div style="outline: solid 1px black;text-align: left">
                    <p class="text-judul" style="margin-left: 5px">KASUBBAG RENMIN</p>
                </div>
            </div>
            <div style="position: absolute;right: 0;width: 49%;top:135px;margin-left: 5px">
                <div style="outline: solid 1px black;text-align: left;height: 98%;">
                </div>
            </div>
        </div>
        <hr style="border-top: 1px dotted black;">
        <div style="height:45%;padding: 10px;display: flex">
        </div>
    </div>
{{--    <div style="height:45%;padding: 10px;display: flex">--}}
{{--        <div style="height:100%;border: 1px solid; position: relative;">--}}
{{--            <div style="position: absolute;width: 40%;">--}}
{{--                <div style="margin-top: 15px;margin-left: 10px;text-align: center">--}}
{{--                    <p class="text-judul">KEPOLISIAN NEGARA REPUBLIK INDONESIA</p>--}}
{{--                    <p class="text-judul">DAERAH SULAWESI SELATAN</p>--}}
{{--                    <p class="text-judul" style="border-bottom: 1px solid black;">BIDANG TEKNOLOGI INFORMASI DAN--}}
{{--                        KOMUNIKASI</p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div style="position: absolute;width: 25%;right: 10px;top: 20px">--}}
{{--                <div style="margin-top: 15px;margin-left: 10px;text-align: left">--}}
{{--                    <p class="text-subjudul">Klasifikasi : Biasa/Rahasia</p>--}}
{{--                    <p class="text-subjudul">Derajat : Kilat</p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div style="position: absolute;width: 100%;top: 70px;display: flex;justify-content: center">--}}
{{--                <p class="text-judul"--}}
{{--                   style="font-weight: bold;border-bottom: 1px solid black;width: 107px;margin: 0 auto">LEMBAR--}}
{{--                    DISPOSISI</p>--}}
{{--            </div>--}}
{{--            <div style="position: absolute;width: 40%;top: 80px">--}}
{{--                <div style="margin-top: 15px;margin-left: 20px;">--}}
{{--                    <p class="text-subjudul">No. Agenda : {{$data->noAgenda}}</p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div style="position: absolute;width: 25%;top: 80px;right: 10px">--}}
{{--                <div style="margin-top: 15px;margin-left: 10px;">--}}
{{--                    <p class="text-subjudul">Diterima : {{$data->tglMasukSurat}}</p>--}}
{{--                    <p class="text-subjudul">Jam : {{substr($data->jamSurat,0,5)}} Wita</p>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div style="width: 50%;margin-top: 125px;margin-left: 5px">--}}
{{--                <div style="outline: solid 1px black;text-align: center">--}}
{{--                    <p class="text-judul">CAT. RENMIN</p>--}}
{{--                </div>--}}
{{--                <div style="outline: solid 1px black;text-align: left;height: 76%;">--}}
{{--                    <p class="text-konten">SURAT DARI : {{$data->pengirim}}</p>--}}
{{--                    <p class="text-konten">NOMOR SURAT : {{$data->noSurat}}</p>--}}
{{--                    <p class="text-konten">TANGGAL SURAT : {{$data->tglPembuatanSurat}}</p>--}}
{{--                    <p class="text-konten">PERIHAL : {{$data->perihal}}</p>--}}
{{--                </div>--}}
{{--                <div style="outline: solid 1px black;text-align: center">--}}
{{--                    <p class="text-judul">Diteruskan kepada:</p>--}}
{{--                </div>--}}
{{--                <div style="outline: solid 1px black;text-align: left">--}}
{{--                    <p class="text-judul" style="margin-left: 5px">KASUBBID TEKKOM</p>--}}
{{--                </div>--}}
{{--                <div style="outline: solid 1px black;text-align: left">--}}
{{--                    <p class="text-judul" style="margin-left: 5px">Plt. KASUBBID TEKINFO</p>--}}
{{--                </div>--}}
{{--                <div style="outline: solid 1px black;text-align: left">--}}
{{--                    <p class="text-judul" style="margin-left: 5px">KASUBBAG RENMIN</p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div style="position: absolute;right: 0;width: 49%;top:135px;margin-left: 5px">--}}
{{--                <div style="outline: solid 1px black;text-align: left;height: 98%;">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <hr style="border-top: 1px dotted black;">--}}
{{--        <div style="height:45%;padding: 10px;display: flex">--}}
{{--        </div>--}}
{{--    </div>--}}
</div>
</body>
</html>
