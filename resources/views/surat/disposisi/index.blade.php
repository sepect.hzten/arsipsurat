@extends('template.app')
@section('content')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-0">Disposisi</h4>
                            <hr>
                            <h5 class="card-title mb-0">Data surat yang perlu disposisi</h5>
                        </div>
                        <div class="card-body">
                            <table id="example"
                                   class="table table-bordered dt-responsive nowrap table-striped align-middle"
                                   style="width:100%">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Tanggal Masuk</th>
                                    <th>Jenis Surat</th>
                                    <th>No. Agenda</th>
                                    <th>No. Surat</th>
                                    <th>Tanggal Surat</th>
                                    <th>Pengirim</th>
                                    <th>Perihal</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $no =1;
                                @endphp
                                @foreach($data as $t)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$t->tglMasukSurat}}</td>
                                        <td>{{$t->rJenis->jenisSurat}}</td>
                                        <td>{{$t->noAgenda}}</td>
                                        <td>
                                            <a href="{{asset('file/surat/'.$t->fileSuratMasuk)}}" target="_blank">
                                                {{$t->noSurat}}
                                            </a>
                                        </td>
                                        <td>{{$t->tglPembuatanSurat}}</td>
                                        <td>{{$t->pengirim}}</td>
                                        <td>{{$t->perihal}}</td>
                                        <td>
                                            <div class="hstack gap-3 fs-base">
                                                <a href="{{url('/disposisi/create/'.$t->idSuratMasuk)}}" class="link-success"
                                                ><i class="ri-upload-line"></i></a>
                                                <a href="{{url('/cetak/disposisi/'.$t->idSuratMasuk)}}" target="_blank"
                                                   class="link-warning"><i class="ri-pencil-line"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!--end col-->
            </div>
        </div>
        <!-- container-fluid -->
    </div>


    <div class="modal fade" id="edit" tabindex="-1" aria-labelledby="edit">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="add">Upload Surat</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{url('upload/suratkeluar')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" id="id" name="id">
                        <div class="row g-3">
                            <!--end col-->
                            <div class="col-xxl-12">
                                <div>
                                    <div>
                                        <label for="file" class="form-label">File Surat Keluar</label>
                                        <input type="file" class="form-control" id="file" name="file"
                                               required>
                                    </div>
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-lg-12">
                                <div class="hstack gap-2 justify-content-end">
                                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Upload</button>
                                </div>
                            </div>
                            <!--end col-->
                        </div>
                        <!--end row-->
                    </form>
                </div>
            </div>
        </div>
    </div>

    @push('script')
        <script src="{{asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script>
            $("#edit").on("show.bs.modal", function (event) {
                let button = $(event.relatedTarget);
                let id = button.data("id");
                let modal = $('#edit');
                modal.find("#id").val(id);
            });
            function delData(id) {
                Swal.fire({
                    title: 'Hapus data',
                    text: "Yakin ingin mengapus data ini?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, hapus data!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.isConfirmed) {
                        axios.delete('{{url('/del/suratkeluar')}}/' + id)
                            .then(function (response) {
                                if (response.data === 1) {
                                    Swal.fire({
                                        title: 'Terhapus',
                                        text: "Data berhasil terhapus.",
                                        icon: 'success',
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'Oke'
                                    }).then((result) => {
                                        location.reload(true);
                                    });
                                } else {
                                    Swal.fire({
                                        title: 'Gagal!',
                                        text: "Ada Kesalahan!",
                                        icon: 'info',
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'Oke'
                                    }).then((result) => {
                                        location.reload(true);
                                    });
                                }
                            });
                    }
                })
            }
            function approveSurat(id) {
                Swal.fire({
                    title: 'Konfirmasi data',
                    text: "Yakin ingin approve data ini?",
                    icon: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, approve data!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.isConfirmed) {
                        axios.get('{{url('/approve/suratkeluar')}}/' + id)
                            .then(function (response) {
                                if (response.data === 1) {
                                    Swal.fire({
                                        title: 'Berhasil',
                                        text: "Data berhasil diapprove.",
                                        icon: 'success',
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'Oke'
                                    }).then((result) => {
                                        location.reload(true);
                                    });
                                } else {
                                    Swal.fire({
                                        title: 'Gagal!',
                                        text: "Ada Kesalahan!",
                                        icon: 'info',
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'Oke'
                                    }).then((result) => {
                                        location.reload(true);
                                    });
                                }
                            });
                    }
                })
            }
            async function declineSurat(id) {
                const {value: text} = await Swal.fire({
                    title: 'Pesan Koreksi Surat',
                    input: "textarea",
                    inputLabel: "Masukkan Komentar",
                    inputPlaceholder: "...",
                    inputAttributes: {
                        "aria-label": ". . ."
                    },
                    showCancelButton: true,
                    confirmButtonText: `Kirim`,
                    cancelButtonText: `Batal`,
                });
                if (text) {
                    axios.get('{{url('/tolak/suratkeluar')}}/' + id + '@' + text)
                        .then(function (response) {
                            if (response.data === 1) {
                                Swal.fire({
                                    title: 'Info',
                                    text: "Data berhasil dikirim.",
                                    icon: 'success',
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: 'Oke'
                                }).then((result) => {
                                    location.reload(true);
                                });
                            } else {
                                Swal.fire({
                                    title: 'Gagal!',
                                    text: "Ada Kesalahan!",
                                    icon: 'info',
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: 'Oke'
                                }).then((result) => {
                                    location.reload(true);
                                });
                            }
                        });
                }
            }
        </script>
    @endpush
@endsection
