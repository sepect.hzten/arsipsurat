@extends('template.app')
@section('content')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Disposisi Surat Masuk</h5>
                        </div>
                        <div class="card-body">
                            <form action="{{url('add/disposisi')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{$data->idSuratMasuk}}">
                                <div class="row g-3">
                                    <div class="col-lg-12">
                                        <div>
                                            <label for="file" class="form-label">File Disposisi</label>
                                            <input type="file" class="form-control" id="file" name="file"
                                                   required>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="col-lg-12">
                                        <div class="text-center">
                                            <p>Teruskan Kepada</p>
                                            <div class="btn-group" role="group" aria-label="Basic checkbox toggle button group">
                                                <input type="checkbox" class="btn-check" id="btncheck1" autocomplete="off" value="RENMIN" name="kepada[]">
                                                <label class="btn btn-outline-primary" for="btncheck1">RENMIN</label>

                                                <input type="checkbox" class="btn-check" id="btncheck2" autocomplete="off" value="TEKKOM" name="kepada[]">
                                                <label class="btn btn-outline-primary" for="btncheck2">TEKKOM</label>

                                                <input type="checkbox" class="btn-check" id="btncheck3" autocomplete="off" value="TEKINFO" name="kepada[]">
                                                <label class="btn btn-outline-primary" for="btncheck3">TEKINFO</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" onclick="history.back()">
                                                Kembali
                                            </button>
                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                        </div>
                                    </div>
                                    <!--end col-->
                                </div>
                                <!--end row-->
                            </form>
                        </div>
                    </div>
                </div><!--end col-->
            </div>
        </div>
        <!-- container-fluid -->
    </div>
@endsection
