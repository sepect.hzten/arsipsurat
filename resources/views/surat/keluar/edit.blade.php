@extends('template.app')
@section('content')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Update Surat Keluar</h5>
                        </div>
                        <div class="card-body">
                            <form action="{{url('edit/suratkeluar')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{$data->idSuratKeluar}}">
                                <div class="row g-3">
                                    <div class="col-lg-12">
                                        <div>
                                            <label for="tglSurat" class="form-label">Tanggal Surat</label>
                                            <input type="date" class="form-control" id="tglSurat" name="tglSurat" value="{{$data->tglSuratKeluar}}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div>
                                            <label for="noSurat" class="form-label">Nomor Surat</label>
                                            <input type="text" class="form-control" id="noSurat" name="noSurat" value="{{$data->noSurat}}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div>
                                            <label for="jenis" class="form-label">Jenis Surat</label>
                                            <select name="jenis" id="jenis" class="form-control" required>
                                                <option value="" disabled>Pilih Jenis Surat</option>
                                                @foreach($jenis as $t)
                                                    <option value="{{$t->idJenisSurat}}" {{$t->idJenisSurat === $data->idJenisSurat ? 'selected' : ''}}>{{$t->jenisSurat}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div>
                                            <label for="kepada" class="form-label">Kepada</label>
                                            <input type="text" class="form-control" id="kepada" name="kepada" value="{{$data->kepada}}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div>
                                            <label for="perihal" class="form-label">Perihal</label>
                                            <input type="text" class="form-control" id="perihal" name="perihal" value="{{$data->perihal}}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div>
                                            <label for="file" class="form-label">File Surat Keluar</label>
                                            <input type="file" class="form-control" id="file" name="file">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" onclick="history.back()">
                                                Kembali
                                            </button>
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </div>
                                    <!--end col-->
                                </div>
                                <!--end row-->
                            </form>
                        </div>
                    </div>
                </div><!--end col-->
            </div>
        </div>
        <!-- container-fluid -->
    </div>
@endsection
