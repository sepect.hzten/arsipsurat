@extends('template.app')
@section('content')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Tambah Surat Masuk</h5>
                        </div>
                        <div class="card-body">
                            <form action="{{url('add/suratmasuk')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row g-3">
                                    <div class="col-lg-12">
                                        <div>
                                            <label for="tglMasuk" class="form-label">Tanggal Masuk Surat</label>
                                            <input type="date" class="form-control" id="tglMasuk" name="tglMasuk"
                                                   value="{{old('tglMasuk')}}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div>
                                            <label for="jam" class="form-label">Jam Masuk Surat</label>
                                            <input type="time" class="form-control" id="jam" name="jam"
                                                   value="{{old('jam')}}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div>
                                            <label for="jenis" class="form-label">Jenis Surat</label>
                                            <select name="jenis" id="jenis" class="form-control" required>
                                                <option value="" selected disabled>Pilih Jenis Surat</option>
                                                @foreach($jenis as $t)
                                                    <option value="{{$t->idJenisSurat}}" {{old('jenis') === $t->idJenisSurat ? 'selected' : ''}}>{{$t->jenisSurat}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div>
                                            <label for="noSurat" class="form-label">Nomor Surat</label>
                                            <input type="text" class="form-control" id="noSurat" name="noSurat"
                                                   value="{{old('noSurat')}}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div>
                                            <label for="tglSurat" class="form-label">Tanggal Surat</label>
                                            <input type="date" class="form-control" id="tglSurat" name="tglSurat"
                                                   value="{{old('tglSurat')}}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div>
                                            <label for="pengirim" class="form-label">Pengirim</label>
                                            <input type="text" class="form-control" id="pengirim" name="pengirim"
                                                   value="{{old('pengirim')}}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div>
                                            <label for="perihal" class="form-label">Perihal</label>
                                            <input type="text" class="form-control" id="perihal" name="perihal"
                                                   value="{{old('perihal')}}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div>
                                            <label for="keterangan" class="form-label">Keterangan</label>
                                            <input type="text" class="form-control" id="keterangan" name="keterangan"
                                                   value="{{old('keterangan')}}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div>
                                            <label for="file" class="form-label">File Surat Masuk</label>
                                            <input type="file" class="form-control" id="file" name="file"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" onclick="history.back()">
                                                Kembali
                                            </button>
                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                        </div>
                                    </div>
                                    <!--end col-->
                                </div>
                                <!--end row-->
                            </form>
                        </div>
                    </div>
                </div><!--end col-->
            </div>
        </div>
        <!-- container-fluid -->
    </div>
@endsection
