@extends('template.app')
@section('content')
    @if(\Illuminate\Support\Facades\Auth::user()->level === 'RENMIN')
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title mb-0">Data Surat Masuk</h5>
                                <hr>
                                <button type="button" class="btn btn-primary" onclick="location.href='{{url('/suratmasuk/create')}}'">
                                    Tambah Surat
                                </button>
                            </div>
                            <div class="card-body">
                                <table id="example"
                                       class="table table-bordered dt-responsive nowrap table-striped align-middle"
                                       style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Tanggal Masuk</th>
                                        <th>Jenis Surat</th>
                                        <th>No. Agenda</th>
                                        <th>No. Surat</th>
                                        <th>Tanggal Surat</th>
                                        <th>Pengirim</th>
                                        <th>Perihal</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $no =1;
                                    @endphp
                                    @foreach($data as $t)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$t->tglMasukSurat}}</td>
                                            <td>{{$t->rJenis->jenisSurat}}</td>
                                            <td>{{$t->noAgenda}}</td>
                                            <td>
                                                <a href="{{asset('file/surat/'.$t->fileSuratMasuk)}}" target="_blank">
                                                    {{$t->noSurat}}
                                                </a>
                                            </td>
                                            <td>{{$t->tglPembuatanSurat}}</td>
                                            <td>{{$t->pengirim}}</td>
                                            <td>{{$t->perihal}}</td>
                                            <td>
                                                <div class="hstack gap-3 fs-base">
                                                    <a href="{{url('/suratmasuk/update/'.$t->idSuratMasuk)}}" class="link-primary"
                                                    ><i class="ri-edit-2-fill"></i></a>
                                                    <a href="javascript:void(0);" onclick="delData('{{$t->idSuratMasuk}}')"
                                                       class="link-danger"><i class="ri-delete-bin-5-line"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!--end col-->
                </div>
            </div>
            <!-- container-fluid -->
        </div>
    @else
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title mb-0">Data Surat Masuk</h5>
                            </div>
                            <div class="card-body">
                                <table id="example"
                                       class="table table-bordered dt-responsive nowrap table-striped align-middle"
                                       style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Tanggal Masuk</th>
                                        <th>Jenis Surat</th>
                                        <th>No. Agenda</th>
                                        <th>No. Surat</th>
                                        <th>Tanggal Surat</th>
                                        <th>Pengirim</th>
                                        <th>Perihal</th>
                                        <th>File</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $no =1;
                                    @endphp
                                    @foreach($data as $t)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$t->tglMasukSurat}}</td>
                                            <td>{{$t->rJenis->jenisSurat}}</td>
                                            <td>{{$t->noAgenda}}</td>
                                            <td>
                                                    {{$t->noSurat}}
                                            </td>
                                            <td>{{$t->tglPembuatanSurat}}</td>
                                            <td>{{$t->pengirim}}</td>
                                            <td>{{$t->perihal}}</td>
                                            <td>
                                                <div class="vstack gap-3 fs-base">
                                                    <a href="{{asset('file/surat/'.$t->fileSuratMasuk)}}" target="_blank" class="link-info"
                                                    ><i class="ri-download-2-line"></i> File Surat</a>
                                                    <a href="{{asset('file/surat/'.$t->rDisposisi->fileDisposisi)}}" target="_blank" class="link-success"
                                                    ><i class="ri-download-2-line"></i> File Disposisi</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!--end col-->
                </div>
            </div>
            <!-- container-fluid -->
        </div>
    @endif

    @push('script')
{{--        <script src="{{asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>--}}
        <script>
            function delData(id) {
                Swal.fire({
                    title: 'Hapus data',
                    text: "Yakin ingin mengapus data ini?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, hapus data!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.isConfirmed) {
                        axios.delete('{{url('/del/suratmasuk')}}/' + id)
                            .then(function (response) {
                                if (response.data === 1) {
                                    Swal.fire({
                                        title: 'Terhapus',
                                        text: "Data berhasil terhapus.",
                                        icon: 'success',
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'Oke'
                                    }).then((result) => {
                                        location.reload(true);
                                    });
                                } else {
                                    Swal.fire({
                                        title: 'Gagal!',
                                        text: "Ada Kesalahan!",
                                        icon: 'info',
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'Oke'
                                    }).then((result) => {
                                        location.reload(true);
                                    });
                                }
                            });
                    }
                })
            }
        </script>
    @endpush
@endsection
