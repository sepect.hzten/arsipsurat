<!DOCTYPE html>
<html lang="en" data-layout="vertical" data-sidebar="dark" data-sidebar-size="lg" data-preloader="disable"
      data-theme="default" data-topbar="light" data-bs-theme="light" data-layout-style="detached"
      data-layout-width="fluid" data-sidebar-image="none" data-layout-position="fixed">
<head>
    @include('template.head')
    @stack('head')
</head>
<body>
<div id="layout-wrapper">
    @include('template.sidebar')

    <div class="vertical-overlay"></div>

    @include('template.header')
    <div class="main-content">
        @yield('content')
        @include('template.footer')
    </div>
</div>

<button class="btn btn-dark btn-icon" id="back-to-top">
    <i class="bi bi-caret-up fs-3xl"></i>
</button>

@include('template.script')
@stack('script')
</body>
</html>
