<div class="app-menu navbar-menu" style="background-color: #613D3D">
    <!-- LOGO -->
    <div class="navbar-brand-box">
        <a href="./" class="logo logo-dark">
                                    <span class="logo-sm">
                                        <img
                                            src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Lambang_Polda_Sulsel.png/640px-Lambang_Polda_Sulsel.png"
                                            alt="" height="40">
                                    </span>
            <span class="logo-lg">
                                        <img
                                            src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Lambang_Polda_Sulsel.png/640px-Lambang_Polda_Sulsel.png"
                                            alt="" height="40">
                                    </span>
        </a>
        <a href="./" class="logo logo-light">
                                    <span class="logo-sm">
                                        <img
                                            src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Lambang_Polda_Sulsel.png/640px-Lambang_Polda_Sulsel.png"
                                            alt="" height="40">
                                    </span>
            <span class="logo-lg">
                                        <img
                                            src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Lambang_Polda_Sulsel.png/640px-Lambang_Polda_Sulsel.png"
                                            alt="" height="40">
                                    </span>
        </a>
        <button type="button" class="btn btn-sm p-0 fs-3xl header-item float-end btn-vertical-sm-hover"
                id="vertical-hover">
            <i class="ri-record-circle-line"></i>
        </button>
    </div>

    <div id="scrollbar">
        <div class="container-fluid">

            <div id="two-column-menu">
            </div>
            <ul class="navbar-nav" id="navbar-nav">


                @if(\Illuminate\Support\Facades\Auth::user()->level === 'RENMIN')
                    <li class="menu-title"><span data-key="t-menu">Menu</span></li>
                    <li class="nav-item">
                        <a class="nav-link menu-link" href="./">
                            <i class="ph-gauge"></i> <span data-key="t-dashboards">Dashboards</span>
                        </a>
                    </li>
                @endif

                <li class="menu-title"><i class="ri-more-fill"></i> <span data-key="t-apps">Surat</span></li>

                <li class="nav-item">
                    <a href="{{url('/suratmasuk')}}" class="nav-link menu-link"> <i class="ph-envelope"></i> <span
                            data-key="t-calendar">Surat Masuk</span> </a>
                </li>

                <li class="nav-item">
                    <a href="{{url('suratkeluar')}}" class="nav-link menu-link"> <i class="ph-arrow-arc-right-fill"></i>
                        <span
                            data-key="t-chat">Surat Keluar</span> </a>
                </li>

                @if(\Illuminate\Support\Facades\Auth::user()->level === 'RENMIN')

                    <li class="nav-item">
                        <a href="{{url('/disposisi')}}" class="nav-link menu-link"> <i class="ph-paper-plane-fill"></i>
                            <span
                                data-key="t-chat">Disposisi Surat</span> </a>
                    </li>

                    <li class="menu-title"><i class="ri-more-fill"></i> <span data-key="t-apps">Master</span></li>


                    <li class="nav-item">
                        <a href="#sidebarEcommerce" class="nav-link menu-link collapsed" data-bs-toggle="collapse"
                           role="button" aria-expanded="false" aria-controls="sidebarEcommerce">
                            <i class="ph-storefront"></i> <span data-key="t-master">Master Data</span>
                        </a>
                        <div class="collapse menu-dropdown" id="sidebarEcommerce">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a href="{{url('/pengguna')}}" class="nav-link" data-key="t-products">Data
                                        Pengguna</a>
                                </li>
{{--                                <li class="nav-item">--}}
{{--                                    <a href="{{url('/satker')}}" class="nav-link" data-key="t-products">Data Satker</a>--}}
{{--                                </li>--}}
                                <li class="nav-item">
                                    <a href="{{url('/jenis')}}" class="nav-link" data-key="t-products">Jenis Surat</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                @endif

                <li class="nav-item">
                    <a href="{{url('/arsip')}}" class="nav-link menu-link"> <i class="ph-archive"></i> <span
                            data-key="t-chat">Arsip Surat</span> </a>
                </li>

                <li class="menu-title"><i class="ri-more-fill"></i> <span data-key="t-apps">Keluar</span></li>

                <li class="nav-item">
                    <a href="{{url('/logout')}}" class="nav-link menu-link"> <i class="ph-lock-key-open"></i> <span
                            data-key="t-email">Log Out</span> </a>
                </li>
            </ul>
        </div>
        <!-- Sidebar -->
    </div>

    <div class="sidebar-background"></div>
</div>
