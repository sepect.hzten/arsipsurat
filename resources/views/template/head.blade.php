
<meta charset="utf-8">
<title>Arsip Surat</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta content="Minimal Admin & Dashboard Template" name="description">
<meta content="Themesbrand" name="author">
<!-- App favicon -->
<link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

<!-- Fonts css load -->
<link rel="preconnect" href="https://fonts.googleapis.com/">
<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
<link id="fontsLink" href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&amp;display=swap" rel="stylesheet">

<!-- jsvectormap css -->
<link href="{{asset('assets/libs/jsvectormap/css/jsvectormap.min.css')}}" rel="stylesheet" type="text/css">

<!--Swiper slider css-->
<link href="{{asset('assets/libs/swiper/swiper-bundle.min.css')}}" rel="stylesheet" type="text/css">

<!--datatable css-->
<link rel="stylesheet" href="{{asset('cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css')}}" >
<!--datatable responsive css-->
<link rel="stylesheet" href="{{asset('cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css')}}" >

<link rel="stylesheet" href="{{asset('cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css')}}">

<!-- Layout config Js -->
<script src="{{asset('assets/js/layout.js')}}"></script>
<!-- Bootstrap Css -->
<link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<!-- Icons Css -->
<link href="{{asset('assets/css/icons.min.css')}}" rel="stylesheet" type="text/css">
<!-- App Css-->
<link href="{{asset('assets/css/app.min.css')}}" rel="stylesheet" type="text/css">
<!-- custom Css-->
<link href="{{asset('assets/css/custom.min.css')}}" rel="stylesheet" type="text/css">
