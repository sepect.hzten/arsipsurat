@extends('template.app')
@section('content')

    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xxl col-sm-6">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="avatar-sm float-end">
                                <div class="avatar-title bg-secondary-subtle text-info fs-3xl rounded">
                                    <i class="ph ph-envelope"></i>
                                </div>
                            </div>
                            <h4><span class="counter-value" data-target="{{$masukC}}">{{$masukC}}</span></h4>
                            <h5 class="text-muted mb-4">Surat Masuk</h5>
                        </div>
                        <div class="progress progress-sm rounded-0" role="progressbar" aria-valuenow="100"
                             aria-valuemin="0" aria-valuemax="100">
                            <div class="progress-bar bg-info" style="width: 100%"></div>
                        </div>
                    </div>
                </div>
                <div class="col-xxl col-sm-6">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="avatar-sm float-end">
                                <div class="avatar-title bg-danger-subtle text-info fs-3xl rounded">
                                    <i class="ph ph-arrow-arc-right-fill"></i>
                                </div>
                            </div>
                            <h4><span class="counter-value" data-target="{{$keluarC}}">{{$keluarC}}</span></h4>
                            <h5 class="text-muted mb-4">Surat Keluar</h5>
                        </div>
                        <div class="progress progress-sm rounded-0" role="progressbar" aria-valuenow="100"
                             aria-valuemin="0" aria-valuemax="100">
                            <div class="progress-bar bg-danger" style="width: 100%"></div>
                        </div>
                    </div>
                </div>
                <div class="col-xxl col-sm-6">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="avatar-sm float-end">
                                <div class="avatar-title bg-success-subtle text-info fs-3xl rounded">
                                    <i class="ph ph-paperclip-light"></i>
                                </div>
                            </div>
                            <h4><span class="counter-value" data-target="{{$disposisiC}}">{{$disposisiC}}</span></h4>
                            <h5 class="text-muted mb-4">Disposisi</h5>
                        </div>
                        <div class="progress progress-sm rounded-0" role="progressbar" aria-valuenow="100"
                             aria-valuemin="0" aria-valuemax="100">
                            <div class="progress-bar bg-success" style="width: 100%"></div>
                        </div>
                    </div>
                </div>
                <div class="col-xxl col-sm-6">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="avatar-sm float-end">
                                <div class="avatar-title bg-warning-subtle text-info fs-3xl rounded">
                                    <i class="ph ph-users"></i>
                                </div>
                            </div>
                            <h4><span class="counter-value" data-target="{{$petugasC}}">{{$petugasC}}</span></h4>
                            <h5 class="text-muted mb-4">Petugas</h5>
                        </div>
                        <div class="progress progress-sm rounded-0" role="progressbar" aria-valuenow="100"
                             aria-valuemin="0" aria-valuemax="100">
                            <div class="progress-bar bg-warning" style="width: 100%"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="card" id="leadsList">
                        <div class="card-header align-items-center d-flex">
                            <h4 class="card-title mb-0 flex-grow-1">Data Surat Terbaru</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive table-card mt-0">
                                <table class="table table-borderless table-centered align-middle mb-0">
                                    <thead class="text-muted table-light">
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal Surat</th>
                                        <th>Status</th>
                                        <th>Jenis Surat</th>
                                        <th>Nomor Surat</th>
                                        <th>Perihal</th>
                                        <th>File</th>
                                    </tr>
                                    </thead>
                                    @php
                                        $no = 1;
                                    @endphp
                                    <tbody class="list">
                                    @foreach($data as $t)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{\Carbon\Carbon::parse($t->created_at)->locale('id')->settings(['formatFunction' => 'translatedFormat'])->format('d F Y')}}</td>
                                            <td>
                                                @if($t->status === 'Masuk')
                                                    <span class="text-success">Masuk</span>
                                                @else
                                                    <span class="text-danger">Keluar</span>
                                                @endif
                                            </td>
                                            <td>{{$t->rJenis->jenisSurat}}</td>
                                            <td>{{$t->noSurat}}</td>
                                            <td>{{$t->perihal}}</td>
                                            <td>
                                                @if($t->rMasuk)
                                                    <a href="{{asset('file/surat/'.$t->rMasuk->fileSuratMasuk)}}" target="_blank">
                                                        {{$t->rMasuk->fileSuratMasuk}}
                                                    </a>
                                                @else
                                                    <a href="{{asset('file/surat/'.$t->rKeluar->fileSuratKeluar)}}" target="_blank">
                                                        {{$t->rKeluar->fileSuratKeluar}}
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody><!-- end tbody -->
                                </table><!-- end table -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- container-fluid -->
    </div>
@endsection
