<header id="page-topbar" style="background-color: #EED600">
    <div class="layout-width">
        <div class="navbar-header">
            <div class="d-flex">
                <!-- LOGO -->
                <div class="navbar-brand-box horizontal-logo">
                    <a href="#" class="logo logo-dark">
                                    <span class="logo-sm">
                            <span class="text-dark fw-bold" style="font-size: 20px">Pengarsipan Surat</span>
                                    </span>
                        <span class="logo-lg">
{{--                                        <img--}}
                            {{--                                            src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Lambang_Polda_Sulsel.png/640px-Lambang_Polda_Sulsel.png"--}}
                            {{--                                            alt="" height="40">--}}
                            <span class="text-dark fw-bold" style="font-size: 20px">Pengarsipan Surat</span>
                                    </span>
                    </a>

                    <a href="#" class="logo logo-light">
                                    <span class="logo-sm">
                                        <img
                                            src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Lambang_Polda_Sulsel.png/640px-Lambang_Polda_Sulsel.png"
                                            alt="" height="40">
                                    </span>
                        <span class="logo-lg">
                                        <img
                                            src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Lambang_Polda_Sulsel.png/640px-Lambang_Polda_Sulsel.png"
                                            alt="" height="40">
                                    </span>
                    </a>
                </div>

                <button type="button"
                        class="btn btn-sm px-3 fs-16 header-item vertical-menu-btn topnav-hamburger shadow-none"
                        id="topnav-hamburger-icon">
                                <span class="hamburger-icon">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </span>
                </button>
            </div>

            <div class="d-flex align-items-center">
                <div class="ms-1 header-item d-none d-sm-flex">
                    <button type="button" class="btn btn-icon btn-topbar btn-ghost-dark rounded-circle"
                            data-toggle="fullscreen">
                        <i class='bi bi-arrows-fullscreen fs-lg'></i>
                    </button>
                </div>

                <div class="dropdown topbar-head-dropdown ms-1 header-item">
                    <button type="button" class="btn btn-icon btn-topbar btn-ghost-dark rounded-circle mode-layout"
                            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="bi bi-sun align-middle fs-3xl"></i>
                    </button>
                    <div class="dropdown-menu p-2 dropdown-menu-end" id="light-dark-mode">
                        <a href="#!" class="dropdown-item" data-mode="light"><i class="bi bi-sun align-middle me-2"></i>
                            Default (light mode)</a>
                        <a href="#!" class="dropdown-item" data-mode="dark"><i class="bi bi-moon align-middle me-2"></i>
                            Dark</a>
                        <a href="#!" class="dropdown-item" data-mode="auto"><i
                                class="bi bi-moon-stars align-middle me-2"></i> Auto (system default)</a>
                    </div>
                </div>

                <div class="dropdown ms-sm-3 header-item topbar-user">
                    <span class="text-start ms-xl-2">
                        <span
                            class="d-xl-inline-block ms-1 fw-medium user-name-text">{{Auth::user()->namaPengguna}}</span>
                        <span
                            class="d-xl-block ms-1 fs-sm user-name-sub-text text-uppercase">{{Auth::user()->level}}</span>
                    </span>
                </div>

                @if(\Illuminate\Support\Facades\Auth::user()->level === 'KABID')
                    <div class="ms-2 header-item d-sm-flex">
                        <a href="/logout">
                        <span class="text-dark ms-xl-2 fw-bold">
                            Log Out</span>
                        </a>
                    </div>
                @endif

            </div>
        </div>
    </div>
</header>
