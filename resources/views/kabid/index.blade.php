@extends('kabid.app')
@section('content')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-0">Arsip Surat</h4>
                            <hr>
                            <div class="row">
                                <div class="col-xl-3 col-sm-3">
                                    <a href="#" onclick="onMasuk()">
                                        <div class="card overflow-hidden bg-info-subtle">
                                            <div class="card-body">
                                                <div class="avatar-sm float-end">
                                                    <div
                                                        class="avatar-title bg-secondary-subtle text-info fs-3xl rounded">
                                                        <i class="ph ph-envelope"></i>
                                                    </div>
                                                </div>
                                                <h4><span class="counter-value"
                                                          data-target="{{$masukC}}">{{$masukC}}</span></h4>
                                                <h5 class="text-muted mb-4">Surat Masuk</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xl-3 col-sm-3">
                                    <a href="#" onclick="onKeluar()">
                                        <div class="card overflow-hidden bg-danger-subtle">
                                            <div class="card-body">
                                                <div class="avatar-sm float-end">
                                                    <div class="avatar-title bg-danger-subtle text-info fs-3xl rounded">
                                                        <i class="ph ph-arrow-arc-right-fill"></i>
                                                    </div>
                                                </div>
                                                <h4><span class="counter-value"
                                                          data-target="{{$keluarC}}">{{$keluarC}}</span></h4>
                                                <h5 class="text-muted mb-4">Surat Keluar</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-header">
                                <form action="{{url('filter/kabid')}}" method="post">
                                    @csrf
                                    <div class="row gy-2 gx-3 mb-3 align-items-center">
                                        <div class="col-sm-auto">
                                            <select class="form-select" id="autoSizingSelect" name="subbid" required>
                                                <option selected value="" disabled>Subbid...</option>
                                                <option value="RENMIN">RENMIN</option>
                                                <option value="TEKKOM">TEKKOM</option>
                                                <option value="TEKINFO">TEKINFO</option>
                                            </select>
                                        </div><!--end col-->
                                        <div class="col-sm-auto">
                                            <select class="form-select" id="autoSizingSelect" name="jenis" required>
                                                <option selected value="" disabled>Jenis Surat...</option>
                                                @foreach($jenis as $t)
                                                    <option value="{{$t->idJenisSurat}}">{{$t->jenisSurat}}</option>
                                                @endforeach
                                            </select>
                                        </div><!--end col-->
                                        {{--                                    <div class="col-sm-auto">--}}
                                        {{--                                        <select class="form-select" id="autoSizingSelect" name="bulan">--}}
                                        {{--                                            <option selected>Bulan...</option>--}}
                                        {{--                                            @for($i=1;$i<=12;$i++)--}}
                                        {{--                                                <option value="{{$i}}">{{$i}}</option>--}}
                                        {{--                                            @endfor--}}
                                        {{--                                        </select>--}}
                                        {{--                                    </div><!--end col-->--}}
                                        {{--                                    <div class="col-sm-auto">--}}
                                        {{--                                        <select class="form-select" id="autoSizingSelect" name="tahun">--}}
                                        {{--                                            <option selected>Tahun...</option>--}}
                                        {{--                                            @for($i=date('Y'); $i>=date('Y')-20; $i-=1)--}}
                                        {{--                                                <option value="{{$i}}">{{$i}}</option>--}}
                                        {{--                                            @endfor--}}
                                        {{--                                        </select>--}}
                                        {{--                                    </div><!--end col-->--}}
                                        <div class="col-sm-auto">
                                            <button type="submit" class="btn btn-primary">Cari</button>
                                        </div><!--end col-->
                                    </div><!--end row-->
                                </form>
                        </div>
                        <div id="masuk" class="card-body">
                            <h4 class="card-title mb-0">Arsip Surat Masuk</h4>
                            <hr>
                            <table id="example"
                                   class="table table-bordered dt-responsive nowrap table-striped align-middle"
                                   style="width:100%">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Tanggal Masuk</th>
                                    <th>No. Agenda</th>
                                    <th>No. Surat</th>
                                    <th>Tanggal Surat</th>
                                    <th>Pengirim</th>
                                    <th>Perihal</th>
                                    <th>File</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $no =1;
                                @endphp
                                @foreach($masuk as $t)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$t->tglMasukSurat}}</td>
                                        <td>{{$t->noAgenda}}</td>
                                        <td>
                                            <a href="{{asset('file/surat/'.$t->fileSuratMasuk)}}" target="_blank">
                                                {{$t->noSurat}}
                                            </a>
                                        </td>
                                        <td>{{$t->tglPembuatanSurat}}</td>
                                        <td>{{$t->pengirim}}</td>
                                        <td>{{$t->perihal}}</td>
                                        <td>
                                            <div class="vstack gap-3 fs-base">
                                                <a href="{{asset('file/surat/'.$t->fileSuratMasuk)}}" target="_blank" class="link-info"
                                                ><i class="ri-download-2-line"></i> File Surat</a>
                                                <a href="{{asset('file/surat/'.$t->rDisposisi->fileDisposisi)}}" target="_blank" class="link-success"
                                                ><i class="ri-download-2-line"></i> File Disposisi</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div id="keluar" class="card-body">
                            <h4 class="card-title mb-0">Arsip Surat Keluar</h4>
                            <hr>
                            <table id="example1"
                                   class="table table-bordered dt-responsive nowrap table-striped align-middle"
                                   style="width:100%">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Tanggal Surat</th>
                                    <th>No. Surat</th>
                                    <th>Jenis Surat</th>
                                    <th>Kepada</th>
                                    <th>Perihal</th>
                                    <th>File</th>
                                    <th>Subbid</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $no =1;
                                @endphp
                                @foreach($keluar as $t)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$t->tglSuratKeluar}}</td>
                                        <td>
                                            <a href="{{asset('file/surat/'.$t->fileSuratKeluar)}}" target="_blank">
                                                {{$t->noSurat}}
                                            </a>
                                        </td>
                                        <td>{{$t->rJenis->jenisSurat}}</td>
                                        <td>{{$t->kepada}}</td>
                                        <td>{{$t->perihal}}</td>
                                        <td>
                                            <div class="vstack gap-3 fs-base">
                                                <a href="{{asset('file/surat/'.$t->fileSuratKeluar)}}" target="_blank" class="link-info"
                                                ><i class="ri-download-2-line"></i> File Surat</a>
                                            </div>
                                        </td>
                                        <td>{{$t->rUser->level}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!--end col-->
            </div>
        </div>
        <!-- container-fluid -->
    </div>

    @push('script')
        <script>
            $('#keluar').hide();
            $('#masuk').show();
            function onMasuk(){
                $('#keluar').hide();
                $('#masuk').show();
            }
            function onKeluar(){
                $('#keluar').show();
                $('#masuk').hide();
            }
        </script>
    @endpush
@endsection
