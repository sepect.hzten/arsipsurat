<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\JenisController;
use App\Http\Controllers\SatkerController;
use App\Http\Controllers\PenggunaController;
use App\Http\Controllers\SuratMasukController;
use App\Http\Controllers\SuratKeluarController;
use App\Http\Controllers\DisposisiController;
use App\Http\Controllers\ArsipController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/login', [LoginController::class, 'index']);
Route::get('/logout', [LoginController::class, 'logout']);
Route::post('signIn', [LoginController::class, 'signIn']);
Route::get('/cetak/disposisi/{id}', [DisposisiController::class, 'cetak']);

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', [HomeController::class, 'index']);

//    kabid
    Route::get('/home', [HomeController::class, 'indexKabid']);
    Route::post('filter/kabid', [HomeController::class, 'filterKabid']);

//    master jenis
    Route::get('/jenis', [JenisController::class, 'index']);
    Route::post('jenis/add', [JenisController::class, 'add']);
    Route::post('jenis/edit', [JenisController::class, 'edit']);
    Route::delete('/jenis/del/{id}', [JenisController::class, 'del']);

//    master satker
    Route::get('/satker', [SatkerController::class, 'index']);
    Route::post('satker/add', [SatkerController::class, 'add']);
    Route::post('satker/edit', [SatkerController::class, 'edit']);
    Route::delete('/satker/del/{id}', [SatkerController::class, 'del']);

//    master satker
    Route::get('/pengguna', [PenggunaController::class, 'index']);
    Route::post('pengguna/add', [PenggunaController::class, 'add']);
    Route::post('pengguna/edit', [PenggunaController::class, 'edit']);
    Route::delete('/pengguna/del/{id}', [PenggunaController::class, 'del']);

//    surat masuk
    Route::get('/suratmasuk', [SuratMasukController::class, 'index']);
    Route::get('/suratmasuk/create', [SuratMasukController::class, 'create']);
    Route::get('/suratmasuk/update/{id}', [SuratMasukController::class, 'update']);
    Route::post('add/suratmasuk', [SuratMasukController::class, 'add']);
    Route::post('edit/suratmasuk', [SuratMasukController::class, 'edit']);
    Route::delete('/del/suratmasuk/{id}', [SuratMasukController::class, 'del']);

//    surat keluar
    Route::get('/suratkeluar', [SuratKeluarController::class, 'index']);
    Route::get('/suratkeluar/create', [SuratKeluarController::class, 'create']);
    Route::get('/suratkeluar/update/{id}', [SuratKeluarController::class, 'update']);
    Route::post('add/suratkeluar', [SuratKeluarController::class, 'add']);
    Route::post('edit/suratkeluar', [SuratKeluarController::class, 'edit']);
    Route::post('upload/suratkeluar', [SuratKeluarController::class, 'editFile']);
    Route::delete('/del/suratkeluar/{id}', [SuratKeluarController::class, 'del']);
    Route::get('/tolak/suratkeluar/{id}', [SuratKeluarController::class, 'tolakSurat']);
    Route::get('/approve/suratkeluar/{id}', [SuratKeluarController::class, 'approveSurat']);

//    disposisi
    Route::get('/disposisi', [DisposisiController::class, 'index']);
    Route::get('/disposisi/create/{id}', [DisposisiController::class, 'create']);
    Route::post('add/disposisi', [DisposisiController::class, 'add']);

//    arsip
    Route::get('/arsip', [ArsipController::class, 'index']);
    Route::post('filter/arsip', [ArsipController::class, 'filterIndex']);
});
